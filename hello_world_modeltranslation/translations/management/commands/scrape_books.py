from django.core.management.base import BaseCommand, CommandError
from translations.models import Book
from dotenv import load_dotenv
import os

load_dotenv()
BOOK_URL = os.environ.get("BOOK_URL")

import requests
from bs4 import BeautifulSoup

class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        url = BOOK_URL

        response = requests.get(url)
        soup = BeautifulSoup(response.content, "html.parser")

        book_data = {
            "english_title": [],
            "french_title": [],
            "english_description": [],
            "french_description": []
        }
        # Iterate through table rows
        for row in soup.find_all('tr'):
            # Iterate through table cells in each row
            cells = row.find_all('td')

            if len(cells[0].text) > 1 and len(cells[1].text) > 1 and len(cells[2].text) > 1 and len(cells[4].text) > 1:
                book_data["english_title"].append(cells[1].text.strip("\n"))
                book_data["french_title"].append(cells[0].text.strip("\n"))
                book_data["english_description"].append(cells[2].text.strip("\n"))
                book_data["french_description"].append(cells[4].text.strip("\n"))
        
        for i in range(len(book_data["english_title"])):
            book = Book.objects.create(
                title_en=book_data["english_title"][i],
                description_en=book_data["english_description"][i],
                title_fr = book_data["french_title"][i],
                description_fr = book_data["french_description"][i]
            )

        
        
