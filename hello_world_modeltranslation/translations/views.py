from django.shortcuts import render
from .models import Book
from django.conf import settings
from django.utils.translation import get_language

from django.shortcuts import redirect

# Create your views here.
def create_book(request):
    input_values = request.GET
    set_languages = settings.LANGUAGES

    book = Book.objects.create(
            title=input_values["title_en"],
            description=input_values["description_en"]
        )
    
    for language in set_languages:
        language_code = language[0]
        title = input_values[f"title_{language_code}"]
        description = input_values[f"description_{language_code}"]
        if len(title) > 0:
            book.title = title
        if len(description) > 0:
            book.description = description
    
    book.save()

    return redirect("book-list")

def show_book_form(request):
    languages = []
    context = {}

    set_lang = settings.LANGUAGES
    default_language = settings.MODELTRANSLATION_FALLBACK_LANGUAGES[0]

    for item in set_lang:
        languages.append(item[0])

    context["language_list"] = languages
    context["default_language"] = default_language
    return render(request, "create.html", context)

def book_list(request):
    books = Book.objects.all()

    context = {"book_list": books}
    return render(request, "book_list.html", context)

def search_list(request):
    query = request.GET.get('q', '')  
    books = Book.objects.filter(
        title__icontains=query,
    )    

    context = {"book_list": books}
    return render(request, "book_list.html", context)

def search_bar(request):
    return render(request, "search.html")